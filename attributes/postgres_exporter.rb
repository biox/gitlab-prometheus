#
# Cookbook Name GitLab::Monitoring
# Attributes:: postgres_exporter
#
default["postgres_exporter"]["dir"] = "/opt/prometheus/postgres_exporter"
default["postgres_exporter"]["log_dir"] = "/var/log/prometheus/postgres_exporter"
default["postgres_exporter"]["version"] = "0.2.1"
default["postgres_exporter"]["db_user"] = "postgres_exporter"

default["postgres_exporter"]["flags"]["extend.query-path"] = "#{node['postgres_exporter']['dir']}/queries.yaml"
