resource_name :prometheus_job
default_action :create
actions :delete

attribute :name,                kind_of: String, name_attribute: true, required: true
attribute :scrape_interval,     kind_of: String
attribute :scrape_timeout,      kind_of: String
attribute :target,              kind_of: [Array, String]
attribute :honor_labels,        kind_of: [TrueClass, FalseClass]
attribute :file_inventory,      kind_of: [TrueClass, FalseClass]
attribute :inventory_file_name, kind_of: String
attribute :params,              kind_of: Hash
attribute :relabel_configs,     kind_of: Array
attribute :metrics_path,        kind_of: String, default: "/metrics"

action :create do
end

action :delete do
end
