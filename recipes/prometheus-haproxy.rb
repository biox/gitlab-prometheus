require "yaml"

package "haproxy"

service "haproxy" do
  supports [:reload]
end

template "/etc/haproxy/haproxy.cfg" do
  source "haproxy-prometheus.cfg.erb"
  mode "0600"
  notifies :reload, "service[haproxy]"
end
