include_recipe "gitlab-prometheus::mtail"

node.default["mtail"]["log_paths"] << "/var/log/gitlab/unicorn/unicorn_stderr.log"

cookbook_file File.join(node["mtail"]["progs_dir"], "unicorn.mtail") do
  source "mtail/unicorn.mtail"
  owner "root"
  group "root"
  mode  "0644"
  notifies :restart, "runit_service[mtail]", :delayed
end
