
default["redis_exporter"]["dir"] = "/opt/prometheus/redis_exporter"
default["redis_exporter"]["log_dir"] = "/var/log/prometheus/redis_exporter"
default["redis_exporter"]["version"] = "0.11.3"
default["redis_exporter"]["binary_url"] = "https://github.com/oliver006/redis_exporter/releases/download/v#{node['redis_exporter']['version']}/redis_exporter-v#{node['redis_exporter']['version']}.linux-amd64.tar.gz"
default["redis_exporter"]["checksum"] = "1c92d0072b7a4871611c270b30fb9d265c67d46ce5be95eb45fb8fcae3efc9d0"
default["redis_exporter"]["chef_vault"] = "gitlab-cluster-base"
default["redis_exporter"]["chef_vault_item"] = "_default"
