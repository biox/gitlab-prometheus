if defined?(ChefSpec)
  def create_prometheus_job(resource_name)
    ChefSpec::Matchers::ResourceMatcher.new(:prometheus_job, :create, resource_name)
  end
end
