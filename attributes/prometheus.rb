#
# Cookbook Name GitLab::Monitoring
# Attributes:: prometheus
#
default["prometheus"]["checksum"]    = "4779d5cf08c50ed368a57b102ab3895e5e830d6b355ca4bfecf718a034a164e0"
default["prometheus"]["dir"]         = "/opt/prometheus/prometheus"
default["prometheus"]["alerting_rules_dir"] = "#{node['prometheus']['dir']}/alerts"
default["prometheus"]["recording_rules_dir"] = "#{node['prometheus']['dir']}/recordings"
default["prometheus"]["console_templates_dir"] = "#{node['prometheus']['dir']}/consoles"
default["prometheus"]["inventory_dir"] = "/opt/prometheus/prometheus/inventory"
default["prometheus"]["binary"]      = "#{node['prometheus']['dir']}/prometheus"
default["prometheus"]["log_dir"]     = "/var/log/prometheus/prometheus"
default["prometheus"]["version"]     = "1.7.1"
default["prometheus"]["binary_url"]  = "https://github.com/prometheus/prometheus/releases/download/v#{node['prometheus']['version']}/prometheus-#{node['prometheus']['version']}.linux-amd64.tar.gz"

default["prometheus"]["scrape_interval"] = "15s"
default["prometheus"]["scrape_timeout"] = "10s"
default["prometheus"]["evaluation_interval"] = "15s"

default["prometheus"]["alertmanager"]["port"]            = "9093"
default["prometheus"]["alertmanager"]["inventory"]       = "#{node['prometheus']['dir']}/alertmanagers.yml"

default["prometheus"]["runbooks"]["git_http"]            = "https://gitlab.com/gitlab-com/runbooks.git"
default["prometheus"]["runbooks"]["branch"]              = "master"

default["prometheus"]["flags"]["config.file"]                            = "#{node['prometheus']['dir']}/prometheus.yml"
default["prometheus"]["flags"]["storage.local.chunk-encoding-version"]   = "2"
default["prometheus"]["flags"]["storage.local.path"]                     = "#{node['prometheus']['dir']}/data"
default["prometheus"]["flags"]["storage.local.retention"]                = "8760h0m0s"
default["prometheus"]["flags"]["storage.local.series-file-shrink-ratio"] = "0.4"
default["prometheus"]["flags"]["storage.local.target-heap-size"]         = (node["memory"]["total"].to_i * 0.50 * 1024).to_i.to_s
default["prometheus"]["flags"]["web.console.libraries"]                  = "#{node['prometheus']['dir']}/console_libraries"
default["prometheus"]["flags"]["web.console.templates"]                  = "#{node['prometheus']['dir']}/consoles"
default["prometheus"]["flags"]["web.external-url"]                       = "https://#{node['fqdn']}"

default["prometheus"]["install_method"] = "binary"

default["prometheus"]["jobs"] = []
