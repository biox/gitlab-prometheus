# The MIT License (MIT)
#
# Copyright © 2017 SoundCloud Ltd.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

require "chef/handler"
require "prometheus/client/push"

class PrometheusHandler < Chef::Handler
  attr_reader :textfile, :registry

  JOB = "chef_client".freeze

  def initialize(textfile)
    @textfile = textfile
    @registry = Prometheus::Client.registry
  end

  def report
    # Add extra labels from chef node attributes here.
    labels = {}

    collect_time_metrics(labels)
    collect_role_metrics(labels)
    exception ? collect_error_metrics(labels) : collect_resource_metrics(labels)

    File.write(textfile[:textfile], Prometheus::Client::Formats::Text.marshal(registry))
  rescue => ex
    Chef::Log.error("PrometheusHandler: #{ex.inspect}")
  end

  private

  def collect_time_metrics(labels)
    registry
      .gauge(:chef_client_duration_seconds,
             "The duration of chef-client run in seconds.")
      .set(labels, run_status.elapsed_time)

    registry
      .gauge(:chef_client_last_run_timestamp_seconds,
             "The unix timestamp of the finish of the last chef-client run.")
      .set(labels, run_status.end_time.to_i)
  end

  def collect_error_metrics(labels)
    registry
      .gauge(:chef_client_error, "The bool error status of the last chef-client run.")
      .set(labels, 1)
  end

  def collect_resource_metrics(labels)
    registry
      .gauge(:chef_client_resources,
            "The number of all resources in the run context.")
      .set(labels, run_status.all_resources.size)

    registry
      .gauge(:chef_client_updated_resources,
            "The number of all updated resources in the run context.")
      .set(labels, run_status.updated_resources.size)
  end

  def collect_role_metrics(labels)
    roles = registry.gauge(
      :chef_client_roles,
      "The list of all roles currently applied to the node.")

    node.roles.each do |role|
      roles.set({ role: role }.merge(labels), 1)
    end
  end
end
