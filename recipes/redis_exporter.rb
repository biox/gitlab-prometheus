include_recipe "chef-vault"

vault = chef_vault_item(node["redis_exporter"]["chef_vault"], node["redis_exporter"]["chef_vault_item"])
redis_pw = vault["omnibus-gitlab"]["gitlab_rb"]["redis"]["password"]

directory node["redis_exporter"]["log_dir"] do
  owner node["prometheus"]["user"]
  group node["prometheus"]["group"]
  mode "0755"
  recursive true
end

include_recipe "ark::default"

%w( curl tar bzip2 ).each do |pkg|
  package pkg
end

ark "redis_exporter" do
  url node["redis_exporter"]["binary_url"]
  checksum node["redis_exporter"]["checksum"]
  prefix_root Chef::Config["file_cache_path"]
  path node["redis_exporter"]["dir"]
  creates "redis_exporter"
  owner node["prometheus"]["user"]
  group node["prometheus"]["group"]
  action :dump
end

include_recipe "runit::default"
runit_service "redis_exporter" do
  options(password: redis_pw)
  default_logger true
  log_dir node["redis_exporter"]["log_dir"]
end
