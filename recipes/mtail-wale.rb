include_recipe "gitlab-prometheus::mtail"

node.default["mtail"]["log_paths"] << "/var/log/gitlab/postgresql/current"

cookbook_file "#{node['mtail']['progs_dir']}/wale.mtail" do
  source "mtail/wale.mtail"
  notifies :restart, "runit_service[mtail]", :delayed
end
